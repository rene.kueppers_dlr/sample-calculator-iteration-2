<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

# Sample Calculator

Sample Calculator is a command line tool to calculate characteristic values of a sample.

It provides the following features:
- Reading sample values from command line and CSV (Colon Separated Values) files.
- Performing statistic calculations such as average, variance, and deviation.
- Configurable logging of results and interim results.
- Easy integration of new input sources and calculations.

## Install and Usage

Please see the [user guide](docs/src/user-guide.rst) for information about installation and usage.

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md) if you want to contribute.

## Changes

Please see the [Changelog](CHANGELOG.md) for notable changes.

## Citation

If you use this work in a research publication,
please cite the specific version that you used using the citation metadata on Zenodo [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.ZENODO-DOI.svg)](https://doi.org/10.5281/zenodo.ZENODO-DOI).

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
